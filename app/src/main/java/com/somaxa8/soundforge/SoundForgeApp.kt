package com.somaxa8.soundforge

import android.app.Application
import android.content.Context

class SoundForgeApp : Application() {

    override fun onCreate() {
        super.onCreate()
        instance = this
    }

    companion object {
        private lateinit var instance: SoundForgeApp

        fun getContext(): Context {
            return instance.applicationContext
        }
    }
}