package com.somaxa8.soundforge.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.somaxa8.soundforge.databinding.ItemLibraryBinding
import com.somaxa8.soundforge.domain.model.SoundLibrary

class SoundLibraryAdapter(
    private val libraries: MutableList<SoundLibrary>,
): RecyclerView.Adapter<SoundLibraryAdapter.SoundLibraryViewHolder>() {

    class SoundLibraryViewHolder(
        private val binding: ItemLibraryBinding,
    ): RecyclerView.ViewHolder(binding.root) {

        fun bind(library: SoundLibrary) {
            binding.libraryTitle.text = library.name
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SoundLibraryViewHolder {
        val binding = ItemLibraryBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )

        val holder = SoundLibraryViewHolder(binding)

        holder.itemView.setOnClickListener {
            onItemClick(holder.adapterPosition)
        }

        return holder
    }

    override fun onBindViewHolder(holder: SoundLibraryViewHolder, position: Int) {
        holder.bind(libraries[position])
    }

    override fun getItemCount(): Int = libraries.size

    var onItemClick: (Int) -> Unit = {}

}