package com.somaxa8.soundforge.domain.model

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class JwtToken(
    var token: String
)