package com.somaxa8.soundforge.domain.model

sealed class LoginResponse {
    data object Error : LoginResponse()
    data class Success(val token: String) : LoginResponse()
}