package com.somaxa8.soundforge.domain.model

sealed class MultipleResponse<T> {
    data class Error<T>(val errorMessage: String) : MultipleResponse<T>()
    data class Success<T>(val data: MutableList<T>) : MultipleResponse<T>()
}