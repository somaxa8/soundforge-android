package com.somaxa8.soundforge.domain.model

sealed class SingleResponse<T> {
    data class Error<T>(val errorMessage: String) : SingleResponse<T>()
    data class Success<T>(val data: T) : SingleResponse<T>()
}