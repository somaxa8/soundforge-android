package com.somaxa8.soundforge.domain.model

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class SoundLibrary(
    var id: Long? = null,

    var name: String? = null,

    var description: String? = null,

    var published: Boolean? = null,
)