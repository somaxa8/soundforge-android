package com.somaxa8.soundforge.domain.model

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class UserCredentials(
    var email: String,

    var password: String
)