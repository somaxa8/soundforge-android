package com.somaxa8.soundforge.domain.usecase

import com.somaxa8.soundforge.domain.model.MultipleResponse
import com.somaxa8.soundforge.domain.model.SoundLibrary
import com.somaxa8.soundforge.service.SoundLibraryService
import com.somaxa8.soundforge.service.utils.handleResponse

class GetSoundLibraryUseCase {

    suspend operator fun invoke(): MultipleResponse<SoundLibrary> = runCatching {
        val params = mapOf(
            "page" to "0",
            "pageSize" to "10"
        )
        SoundLibraryService.api.getAll(params)
    }.handleResponse()
}