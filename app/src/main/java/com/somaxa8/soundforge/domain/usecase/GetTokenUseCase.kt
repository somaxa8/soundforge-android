package com.somaxa8.soundforge.domain.usecase

import com.somaxa8.soundforge.service.SecurityPreferencesService

class GetTokenUseCase {

    operator fun invoke(): String? {
        return SecurityPreferencesService.getToken()
    }
}