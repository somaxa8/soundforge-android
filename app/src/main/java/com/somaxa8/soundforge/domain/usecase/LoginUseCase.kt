package com.somaxa8.soundforge.domain.usecase

import com.somaxa8.soundforge.domain.model.LoginResponse
import com.somaxa8.soundforge.domain.model.UserCredentials
import com.somaxa8.soundforge.service.AuthenticationService

class LoginUseCase {

    suspend operator fun invoke(userCredentials: UserCredentials): LoginResponse {
        return AuthenticationService.login(userCredentials)
    }
}