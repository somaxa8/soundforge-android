package com.somaxa8.soundforge.domain.usecase

import com.somaxa8.soundforge.service.SecurityPreferencesService

class SaveTokenUseCase {

    operator fun invoke(token: String) {
        SecurityPreferencesService.saveToken(token)
    }
}