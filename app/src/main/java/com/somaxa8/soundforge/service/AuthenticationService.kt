package com.somaxa8.soundforge.service

import com.somaxa8.soundforge.domain.model.JwtToken
import com.somaxa8.soundforge.domain.model.LoginResponse
import com.somaxa8.soundforge.domain.model.UserCredentials
import com.somaxa8.soundforge.service.api.AuthenticationApi

object AuthenticationService {

    private val api: AuthenticationApi by lazy {
        RetrofitProvider.createService()
    }

    suspend fun login(userCredentials: UserCredentials): LoginResponse = runCatching {
        api.login(userCredentials)
    }.toLoginResult()

    private fun Result<JwtToken>.toLoginResult(): LoginResponse {
        return when(val result = getOrNull()) {
            null -> {
                LoginResponse.Error
            }
            else -> {
                LoginResponse.Success(token = result.token)
            }
        }
    }
}