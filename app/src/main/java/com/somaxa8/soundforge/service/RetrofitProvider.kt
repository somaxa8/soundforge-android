package com.somaxa8.soundforge.service

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

object RetrofitProvider {

    private const val BASE_URL = "http://192.168.0.23:8080/"

    private val moshi = Moshi.Builder()
        .add(KotlinJsonAdapterFactory())
        .build()

    fun provideRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(getClient())
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .build()
    }

    inline fun <reified T> createService(): T {
        val retrofit = provideRetrofit()
        return retrofit.create(T::class.java)
    }

    private fun getClient(): OkHttpClient {
        val jwtToken = SecurityPreferencesService.getToken()

        return OkHttpClient.Builder().addInterceptor { chain ->
            var request = chain.request()

            jwtToken?.let { token ->
                request = request
                .newBuilder()
                .addHeader("Authorization", "Bearer $token")
                .build()
            }

            chain.proceed(request)
        }.build()
    }
}