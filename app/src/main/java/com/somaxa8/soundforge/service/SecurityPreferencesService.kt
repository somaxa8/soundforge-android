package com.somaxa8.soundforge.service

import android.content.SharedPreferences
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKeys
import com.somaxa8.soundforge.SoundForgeApp

object SecurityPreferencesService {
    private const val TOKEN_KEY = "security.token"

    private val sharedPreferences: SharedPreferences by lazy {
        val context = SoundForgeApp.getContext()

        val masterKeyAlias = MasterKeys.getOrCreate(MasterKeys.AES256_GCM_SPEC)

        EncryptedSharedPreferences.create(
            "security_preferences",
            masterKeyAlias,
            context,
            EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
            EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM,
        )
    }

    fun saveToken(token: String) {
        sharedPreferences.edit().apply {
            putString(TOKEN_KEY, token)
        }.apply()
    }

    fun getToken(): String? {
        return sharedPreferences.getString(TOKEN_KEY, null)
    }
}