package com.somaxa8.soundforge.service

import com.somaxa8.soundforge.service.api.SoundLibraryApi

object SoundLibraryService {

    val api: SoundLibraryApi by lazy {
        RetrofitProvider.createService()
    }

}