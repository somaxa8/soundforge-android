package com.somaxa8.soundforge.service.api

import com.somaxa8.soundforge.domain.model.JwtToken
import com.somaxa8.soundforge.domain.model.UserCredentials
import retrofit2.http.Body
import retrofit2.http.POST

interface AuthenticationApi {

    @POST("/auth/login")
    suspend fun login(
        @Body userCredentials: UserCredentials
    ): JwtToken
}