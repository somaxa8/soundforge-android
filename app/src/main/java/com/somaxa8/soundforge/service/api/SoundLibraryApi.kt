package com.somaxa8.soundforge.service.api

import com.somaxa8.soundforge.domain.model.SoundLibrary
import retrofit2.http.Body
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.Path
import retrofit2.http.QueryMap

interface SoundLibraryApi {

    @GET("/sound-libraries/{id}")
    suspend fun getById(
        @Path("id") id: Long,
    ): SoundLibrary

    @GET("/sound-libraries")
    suspend fun getAll(
        @QueryMap params: Map<String, String>,
    ): MutableList<SoundLibrary>

    @POST("/sound-libraries")
    suspend fun create(
        @Body requestBody: SoundLibrary,
    ): SoundLibrary

    @PUT("/sound-libraries/{id}")
    suspend fun update(
        @Path("id") id: Long,
        @Body requestBody: SoundLibrary,
    ): SoundLibrary

    @DELETE("/sound-libraries/{id}")
    suspend fun delete(
        @Path("id") id: Long,
    ): Nothing

}