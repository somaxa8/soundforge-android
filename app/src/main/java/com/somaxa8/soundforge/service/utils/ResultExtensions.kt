package com.somaxa8.soundforge.service.utils

import com.somaxa8.soundforge.domain.model.MultipleResponse
import com.somaxa8.soundforge.domain.model.SingleResponse


fun <T> Result<MutableList<T>>.handleResponse(): MultipleResponse<T> {
    return when(val result = getOrNull()) {
        null -> {
            MultipleResponse.Error("error")
        }
        else -> {
            MultipleResponse.Success(data = result)
        }
    }
}

fun <T> Result<T>.handleResponse(): SingleResponse<T> {
    return when(val result = getOrNull()) {
        null -> {
            SingleResponse.Error("error")
        }
        else -> {
            SingleResponse.Success(data = result)
        }
    }
}
