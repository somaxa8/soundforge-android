package com.somaxa8.soundforge.ui

import android.content.Intent
import android.os.Bundle
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.somaxa8.soundforge.R
import com.somaxa8.soundforge.adapter.SoundLibraryAdapter
import com.somaxa8.soundforge.databinding.ActivityLoginBinding
import com.somaxa8.soundforge.databinding.ActivityMainBinding
import com.somaxa8.soundforge.domain.model.SoundLibrary
import com.somaxa8.soundforge.domain.usecase.GetTokenUseCase
import com.somaxa8.soundforge.ui.login.LoginActivity

class MainActivity : AppCompatActivity() {

    private var _binding: ActivityMainBinding? = null
    private val binding get() = _binding!!

    private val getTokenUseCase = GetTokenUseCase()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        _binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }

        checkAuthentication()
        setupNavigation()
    }

    private fun checkAuthentication() {
        val result = getTokenUseCase()
        val isAuthenticated = result !== null

        if (!isAuthenticated) {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }
    }

    private fun setupNavigation() {
        val navView: BottomNavigationView = binding.navView
        val navHostFragment = supportFragmentManager
            .findFragmentById(R.id.nav_host_fragment_activity_home) as NavHostFragment
        val navController = navHostFragment.navController

        AppBarConfiguration(
            setOf(
                R.id.navigation_home, R.id.navigation_search, R.id.navigation_profile,
            )
        )

        navView.setupWithNavController(navController)
    }
}