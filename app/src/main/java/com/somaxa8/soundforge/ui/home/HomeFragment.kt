package com.somaxa8.soundforge.ui.home

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import com.somaxa8.soundforge.R
import com.somaxa8.soundforge.adapter.SoundLibraryAdapter
import com.somaxa8.soundforge.databinding.FragmentHomeBinding
import com.somaxa8.soundforge.domain.model.SoundLibrary

class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!

    private val homeViewModel: HomeViewModel by viewModels()
    private var library = mutableListOf<SoundLibrary>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val root = binding.root

        val adapter = SoundLibraryAdapter(library)
        adapter.onItemClick = {
            println("Position $it")
        }
        binding.libraries.adapter = adapter

        initObservers()
        getLibrary()

        return root
    }

    private fun initObservers() {
        homeViewModel.updateLibrary.observe(viewLifecycleOwner) {
            library.addAll(it)
            binding.libraries.adapter?.notifyItemChanged(library.lastIndex)
        }

        homeViewModel.showErrorToast.observe(viewLifecycleOwner) {
            if (it) {
                showErrorToast()
            }
        }
    }

    private fun getLibrary() {
        homeViewModel.getLibrary()
    }

    private fun showErrorToast() {
        Toast.makeText(
            context,
            "Error al obtener las librerias",
            Toast.LENGTH_SHORT
        ).show()
    }

}