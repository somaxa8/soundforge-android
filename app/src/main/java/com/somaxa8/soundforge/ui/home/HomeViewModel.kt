package com.somaxa8.soundforge.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.somaxa8.soundforge.domain.model.MultipleResponse
import com.somaxa8.soundforge.domain.model.SoundLibrary
import com.somaxa8.soundforge.domain.usecase.GetSoundLibraryUseCase
import kotlinx.coroutines.launch

class HomeViewModel : ViewModel() {
    private var _updateLibrary = MutableLiveData(mutableListOf<SoundLibrary>())
    val updateLibrary: LiveData<MutableList<SoundLibrary>> get() = _updateLibrary

    private var _showErrorToast = MutableLiveData(false)
    val showErrorToast: LiveData<Boolean> get() = _showErrorToast

    val getSoundLibraryUseCase = GetSoundLibraryUseCase()

    fun getLibrary() {
        viewModelScope.launch {
            when(val result = getSoundLibraryUseCase()) {
                is MultipleResponse.Error -> {
                    _showErrorToast.value = true
                }
                is MultipleResponse.Success -> {
                    _updateLibrary.value = result.data
                }
            }
        }
    }
}