package com.somaxa8.soundforge.ui.login

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import com.somaxa8.soundforge.R
import com.somaxa8.soundforge.databinding.ActivityLoginBinding
import androidx.activity.viewModels
import androidx.core.widget.addTextChangedListener
import com.somaxa8.soundforge.ui.MainActivity

class LoginActivity : AppCompatActivity() {

    private var _binding: ActivityLoginBinding? = null
    private val binding get() = _binding!!

    private val loginViewModel: LoginViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        _binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }

        initListeners()
        initObservers()
    }

    private fun initListeners() {
        setDebugCredentialsListener()

        binding.loginButton.setOnClickListener {
            login()
        }
        binding.emailInput.addTextChangedListener {
            onFieldChanged()
        }
        binding.passwordInput.addTextChangedListener {
            onFieldChanged()
        }
    }

    private fun initObservers() {
        loginViewModel.showErrorToast.observe(this) {
            if (it) {
                showErrorToast()
            }
        }

        loginViewModel.loginIsSuccess.observe(this) {
            if (it) {
                goToMainActivity()
            }
        }
    }

    private fun showErrorToast() {
        Toast.makeText(
            this,
            "Error al autenticarse",
            Toast.LENGTH_SHORT
        ).show()
    }

    private fun onFieldChanged() {
        loginViewModel.onFieldsChanged(
            email = binding.emailInput.text.toString(),
            password = binding.passwordInput.text.toString(),
        )
    }

    private fun login() {
        loginViewModel.onLoginSelected(
            binding.emailInput.text.toString(),
            binding.passwordInput.text.toString(),
        )
    }

    private fun setDebugCredentialsListener() {
        var setCredentials = 0

        binding.accessYourAccount.setOnClickListener {
            setCredentials += 1
            if (setCredentials == 5) {
                binding.emailInput.setText("silviofrancoxa8@gmail.com")
                binding.passwordInput.setText("1234")
                setCredentials = 0
            }
        }
    }

    private fun goToMainActivity() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }

}