package com.somaxa8.soundforge.ui.login

import android.util.Patterns
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import androidx.lifecycle.viewModelScope
import com.somaxa8.soundforge.domain.model.LoginResponse
import com.somaxa8.soundforge.domain.model.UserCredentials
import com.somaxa8.soundforge.domain.usecase.LoginUseCase
import com.somaxa8.soundforge.domain.usecase.SaveTokenUseCase
import kotlinx.coroutines.launch

class LoginViewModel : ViewModel() {
    private companion object {
        const val MIN_PASSWORD_LENGTH = 4
    }

    private val loginUseCase = LoginUseCase()
    private val saveTokenUseCase = SaveTokenUseCase()

    private val _loginViewState = MutableStateFlow(LoginViewState())
    val loginViewState: StateFlow<LoginViewState>
        get() = _loginViewState

    private var _showErrorToast = MutableLiveData(false)
    val showErrorToast: LiveData<Boolean> get() = _showErrorToast

    private var _loginIsSuccess = MutableLiveData(false)
    val loginIsSuccess : LiveData<Boolean> get() = _loginIsSuccess


    private fun loginUser(email: String, password: String) {
        viewModelScope.launch {
            _loginViewState.value = LoginViewState(isLoading = true)
            val userCredentials = UserCredentials(email, password)

            when (val result = loginUseCase(userCredentials)) {
                LoginResponse.Error -> {
                    _showErrorToast.value = true
                    _loginViewState.value = LoginViewState(isLoading = false)
                }
                is LoginResponse.Success -> {
                    saveTokenUseCase(result.token)
                    _loginIsSuccess.value = true
                }
            }
            _loginViewState.value = LoginViewState(isLoading = false)
        }
    }

    fun onLoginSelected(email: String, password: String) {
        if (isValidEmail(email) && isValidPassword(password)) {
            loginUser(email, password)
        } else {
            onFieldsChanged(email, password)
        }
    }

    fun onFieldsChanged(email: String, password: String) {
        _loginViewState.value = LoginViewState(
            isValidEmail = isValidEmail(email),
            isValidPassword = isValidPassword(password)
        )
    }

    private fun isValidEmail(email: String): Boolean {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches() || email.isEmpty()
    }

    private fun isValidPassword(password: String): Boolean {
        return password.length >= MIN_PASSWORD_LENGTH || password.isEmpty()
    }
}